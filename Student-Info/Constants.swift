//
//  Constants.swift
//  Student-Info
//
//  Created by Apple on 07/07/21.
//  Copyright © 2021 Vaibhav. All rights reserved.
//

import Foundation
struct K{
    static let cellNibName = "StudentCellControllerTableViewCell"
    static let cellIdentifier = "StudentReusableCell"
    static let detailViewControllerIdentifier = "DetailViewController"
    static let key = "student_data"
}
