//
//  StudentCellControllerTableViewCell.swift
//  Student-Info
//
//  Created by Apple on 07/07/21.
//  Copyright © 2021 Vaibhav. All rights reserved.
//

import UIKit

class StudentCellControllerTableViewCell: UITableViewCell {

   
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var gradeLabel: UILabel!
    @IBOutlet weak var rollLabel: UILabel!
    @IBOutlet weak var classLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var semLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func setStudents(student : Student){
        nameLabel.text = student.name
        classLabel.text = student.course
        rollLabel.text = String(student.rollNo)
        gradeLabel.text = String(student.grade)
        semLabel.text = String(student.sem)
        profileImage.image = UIImage(named: student.image)
    }
    
}
