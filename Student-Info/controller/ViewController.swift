//
//  ViewController.swift
//  Student-Info
//
//  Created by Apple on 06/07/21.
//  Copyright © 2021 Vaibhav. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    var students : [Student] = []
    override func viewWillAppear(_ animated: Bool) {
        navigationController?.isNavigationBarHidden = true
        students = LocalPersistance.getData()
        tableView.reloadData()
    }
    override func viewDidDisappear(_ animated: Bool) {
        navigationController?.isNavigationBarHidden = false
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.navigationBar.tintColor = #colorLiteral(red: 0.4012986422, green: 0.5948640704, blue: 0.5370149016, alpha: 1)
        var list = LocalPersistance.getData()
        if list.count == 0 {
            list = createStudentList()
            LocalPersistance.updateAll(students: list)
        }
        students = list
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(UINib(nibName: K.cellNibName, bundle: nil), forCellReuseIdentifier: K.cellIdentifier)
        
    }
   
    func createStudentList()-> [Student]{
        var students : [Student] = []
        students.append(Student(key : 0 ,name: "Vaibhav Sharma", course: "MCA", grade: 8.5,  sem : 1, rollNo: 181685, image: "profile"))
        students.append(Student(key : 1 ,name: "Shubham Verma", course: "BCA", grade: 6.5,  sem : 2, rollNo: 181685, image: "profile1"))
        students.append(Student(key : 2 ,name: "Jivesh Shrivastav", course: "MSC", grade: 7.6,  sem : 3, rollNo: 181685, image: "profile2"))
        students.append(Student(key : 3 ,name: "Ayush Nandecha", course: "PHD", grade: 9.0, sem : 4, rollNo: 181685, image: "profile3"))
        students.append(Student(key : 4 ,name: "Dilip Soni", course: "BBA", grade: 3.0,  sem : 5, rollNo: 181685, image: "profile4"))
        students.append(Student(key : 5 ,name: "Avinash Malakar", course: "MCA", grade: 5.6,  sem : 2, rollNo: 181685, image: "profile5"))
        students.append(Student(key : 6 ,name: "Himanshu chouhan", course: "BSC", grade: 8.2,  sem : 6, rollNo: 181685, image: "profile6"))
        students.append(Student(key : 7 ,name: "Yogesh Paliwal", course: "BCA", grade: 5.1,  sem : 3, rollNo: 181685, image: "profile7"))
        return students
    }


}
extension ViewController : UITableViewDelegate{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let storyboard = UIStoryboard(name : "Main", bundle : nil)
        let vc = storyboard.instantiateViewController(withIdentifier: K.detailViewControllerIdentifier) as! DetailViewController
        vc.student = students[indexPath.row]
        navigationController?.pushViewController(vc, animated: true)
    }
    
}
extension ViewController : UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return students.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let student = students[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: K.cellIdentifier, for: indexPath) as! StudentCellControllerTableViewCell
        cell.setStudents(student: student)
        cell.topView.layer.cornerRadius = cell.topView.frame.height / 4
        cell.profileImage.layer.cornerRadius = cell.profileImage.frame.height / 2
        return cell
    }
    
    
}

