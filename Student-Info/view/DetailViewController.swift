//
//  DetailViewController.swift
//  Student-Info
//
//  Created by Apple on 07/07/21.
//  Copyright © 2021 Vaibhav. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController {
   
    @IBOutlet weak var editButton: UIButton!
    @IBOutlet weak var semField: UITextField!
    @IBOutlet weak var gradeField: UITextField!
    @IBOutlet weak var rollnoField: UITextField!
    @IBOutlet weak var classField: UITextField!
    @IBOutlet weak var nameField: UITextField!
    @IBOutlet weak var profileImage: UIImageView!
    enum  Mode {
        case view,edit
    }
    var mMode : Mode = .view {
        didSet{
            switch mMode {
            case .view:
                updateData()
                editButton.setTitle("Edit", for: .normal)
                nameField.isUserInteractionEnabled = false
                classField.isUserInteractionEnabled = false
                rollnoField.isUserInteractionEnabled = false
                gradeField.isUserInteractionEnabled = false
                semField.isUserInteractionEnabled = false
            case .edit:
                 showAlertView(title: "Mode Changed", message: "Edit Mode Enabled")
                editButton.setTitle("Update", for: .normal)
                nameField.isUserInteractionEnabled = true
                classField.isUserInteractionEnabled = true
                rollnoField.isUserInteractionEnabled = true
                gradeField.isUserInteractionEnabled = true
                semField.isUserInteractionEnabled = true
            }
        }
    }
    var student : Student!
    override func viewDidLoad() {
        super.viewDidLoad()
        print(student.key)
        setupUiFields()
        profileImage.layer.cornerRadius = profileImage.frame.height / 2

    }
    
    @IBAction func editPressed(_ sender: UIButton) {
        mMode = mMode == .view ? .edit: .view
    }
    func updateData(){
        let newStudent = Student(key: student.key, name: nameField.text ?? student.name, course: classField.text ?? student.course, grade: Double(gradeField.text ?? "0") ?? student.grade, sem: Int(semField.text ?? "0" ) ?? student.sem, rollNo: Int(rollnoField.text ?? "0") ?? student.rollNo , image: student.image)
        LocalPersistance.updateData(student: newStudent, index: student.key)
       showAlertView(title: "Successful !!!", message: "Student Details Updated Succesfully")
    }
    func showAlertView(title : String , message : String){
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
               alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (action) in
                   alert.dismiss(animated: true, completion: nil)
               }))
               present(alert,animated: true,completion: nil)
    }
   /* lazy var editBarButton: UIBarButtonItem = {
     @IBAction func editPressed(_ sender: UIButton) {
     }
     let barButtonItem = UIBarButtonItem(barButtonSystemItem: .edit, target: self, action: #selector(didEditButtonClicked(sender :)))
              return barButtonItem
          }()
    
    @objc func didEditButtonClicked(sender: UIBarButtonItem){
        
    }*/
    func setupUiFields(){
        nameField.text = student.name
        classField.text = student.course
        rollnoField.text = String(student.rollNo)
        gradeField.text = String(student.grade)
        semField.text = String(student.sem)
        profileImage.image = UIImage(named: student.image)
    }
}
