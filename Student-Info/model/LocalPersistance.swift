//
//  LocalPersistance.swift
//  Student-Info
//
//  Created by Apple on 07/07/21.
//  Copyright © 2021 Vaibhav. All rights reserved.
//

import Foundation
class LocalPersistance{
    static func updateData(student : Student,index : Int){
       var newData = getData()
        newData[index] = student
        updateAll(students: newData)
    }
    static func updateAll(students :[Student])
    {
        let encoder = JSONEncoder()
                            if let encoded = try? encoder.encode(students){
                               UserDefaults.standard.set(encoded,forKey: K.key)
                                print("done")
                            }
    }
     static func getData()->[Student]{
        let emptyList : [Student] = []
               if let objects = UserDefaults.standard.value(forKey: K.key) as? Data {
                  let decoder = JSONDecoder()
                  if let objectsDecoded = try? decoder.decode(Array.self, from: objects) as [Student] {
                     return objectsDecoded
                  } else {
                    return emptyList
                  }
               } else {
                return emptyList
               }
    }
}
