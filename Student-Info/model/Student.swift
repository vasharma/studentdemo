//
//  Student.swift
//  Student-Info
//
//  Created by Apple on 07/07/21.
//  Copyright © 2021 Vaibhav. All rights reserved.
//

import Foundation
class Student : Codable {
    var key : Int
    var name : String
    var course : String
    var grade : Double
    var sem : Int
    var rollNo : Int
    var image : String
    init(key :Int , name : String ,course : String, grade : Double , sem : Int , rollNo : Int , image : String) {
        self.key = key
        self.name = name
        self.course = course
        self.grade = grade
        self.sem = sem
        self.rollNo = rollNo
        self.image = image
    }
}
